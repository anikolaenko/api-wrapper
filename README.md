# API Wrapper for Augmented Realtime Interface

This is an open-source python API wrapper for 
WordPress UI [metadata recommendation system](https://bitbucket.org/anikolaenko/augmented-realtime-interface). 

You can play with this system [here](http://ari.contextly.com/demo/).

This application is meant to be used as a API starting place for UI recommendation system. This system has 
module based structure. Each module can communicate with API wrapper and request needed data of recommendations, based
on post title or post content.

At this point it supports only two types of recommendations:  metatags and web site URL's. 
This project can be simple extended for return any type of recommendations or other useful data.


# Installation

This API wrapper based on Flask REST API package, so you need to install this python package with few additional packages:

```
    pip install flask-restful
    pip install requests
    pip install wikipedia
```

The easies way for start this wrapper as a service on web server described [here](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-14-04).


# Data format

Input data should be sent to API wrapper in JSON format as request body. Example:
 
```
{
  "data_type": "{module type}",
  "payload": {
    "title": "This is post title",
    "content": "This is post content"
  }
}
``` 

"{module type}" this is the type of requested recommendations. This is an unique module name, should
be something like "METADATA".


Wrapper response also returned in JSON format:

```
{
  "data_type": "{module type}",
  "payload": {module payload}
}
``` 

"{module payload}" this is recommendations response, contain all needed and useful data for 
Augmented Realtime Interface UI.

Response example for data_type "METADATA":

```
{
  "data_type": "METADATA",
  "payload": {
    "tags": [
      "Startups",
      "Silicon Valley"
    ],
    "categories": [
      "Business"
    ]
  }
}
``` 

# Contribution

By default API wrapper is configured for one base url: "/api/recommendations/", but this can be simple changed in file:
"src/api_wrapper.py". 
    
If you want to extend API wrapper with new module, only few things need to be updated:
    
1. Create new class with module implementation like "RecommendationsMetadata". This class has only one required method 
  "get_response()".

2. Map recommendation module class to module name in this factory: 
```
class RecommendationsFactory:
    @staticmethod
    def get(type, payload):
        if type == 'METADATA':
            return RecommendationsMetadata(payload)
        if type == 'WIKIPEDIA':
            return RecommendationsWikipedia(payload)
```


# Supported Platforms

API wrapper requires the python 2.7.


# Problems?

If you find a problem please visit the [issue tracker](https://bitbucket.org/anikolaenko/api-wrapper/issues) and report the issue.

Please be kind and search to see if the issue is already logged before creating a new one. If you're pressed for time, log it anyways.

When creating an issue, please clearly explain:

* What you were trying to do.
* What you expected to happen.
* What actually happened.
* Steps to reproduce the problem.
* Also include any other information you think is relevant to reproduce the problem (such as screenshots or log errors).

# License

Licensed under the MIT.

# Contact

Want to get in touch, ask a question, our email address is <info@contextly.com>.

Thanks for your interest,
Contextly Inc
