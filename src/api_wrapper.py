# Script for running Flask process and listen for connections

from flask import Flask
from flask import request
from flask_restful import Resource, Api, abort
from calais.calais import Calais
from wiki.wiki import Wiki

# Main wrapper class
class APIWrapper(Resource):

    # GET request method
    def get(self):
        # Read request JSON
        request_json = request.get_json()
        recommendation = None

        application.logger.info(request_json)

        if request_json and 'data_type' in request_json:
            recommendation = RecommendationsFactory.get(request_json['data_type'], request_json['payload'])

        if recommendation:
            response = recommendation.get_response()

            application.logger.info(response)

            return response
        else:
             abort(403, message="Recommendation type is not exists")


# Factory for create recommendation object by data_type field
class RecommendationsFactory:
    @staticmethod
    def get(type, payload):
        if type == 'METADATA':
            return RecommendationsMetadata(payload)
        if type == 'WIKIPEDIA':
            return RecommendationsWikipedia(payload)

# Implementation of base recommendations class
class RecommendationsBase:

    def __init__(self, payload):
        self.payload = payload

    def get_response(self):
        raise Exception('Please implement "get_response()" method in your module.')

# Implementation of metadata recommendations
class RecommendationsMetadata(RecommendationsBase):

    def get_response(self):
        calais = Calais()
        result = calais.process(self.payload['content'])

        response = {'data_type': 'METADATA', 'payload': None}

        if result:
            response['payload'] = {
                'tags': result.get_tags(),
                'categories': result.get_categories()
            }

        return response

# Implementation of wikipedia recommendations
class RecommendationsWikipedia(RecommendationsBase):

    def get_response(self):
        calais = Calais()
        result = calais.process(self.payload['content'])

        response = {'data_type': 'WIKIPEDIA', 'payload': None}

        if result:
            wiki = Wiki(result.get_tags() + result.get_categories())
            wiki_items = wiki.get_items()

            if wiki_items and len(wiki_items) > 0:
                response['payload'] = wiki_items

        return response


application = Flask(__name__)
api = Api(application)
api.add_resource(APIWrapper, '/api/recommendations/')

if __name__ == '__main__':
    application.run(debug=True, host='0.0.0.0')
