__author__ = 'andrew'

import wikipedia

# Get basic wiki info for array of terms
class Wiki:

    def __init__(self, terms):
        self.terms = terms

    def get_items(self, summary_characters=600):
        items = []

        for term in self.terms:
            try:
                wiki_term = wikipedia.page(term.replace('_', ' '))
                summary = wiki_term.summary

                # Limit summary characters
                if len(summary) > summary_characters:
                    summary = summary[0:summary_characters] + '...'

                items.append({
                    'name': term,
                    'url': wiki_term.url,
                    'summary': summary
                })
            except Exception, e:
                print 'Wiki page is not exists ', e
        return items
