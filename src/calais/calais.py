# Module for connect to Calais API and get some tags and categories

import requests
import json

CALAIS_ACCESS_TOKEN = "UXmYswDGCNSuLlmnXUviv6E4OqAzAYsa"
CALAIS_URL = "https://api.thomsonreuters.com/permid/calais"

# Connect to Calais API and get some results
class Calais:

    def __init__(self, access_token=CALAIS_ACCESS_TOKEN):
        self.access_token = access_token
        self.json = None

    def process(self, text):
        text = text.encode("ascii", "ignore")

        try:
            headers = {'X-AG-Access-Token': self.access_token, 'Content-Type': 'text/raw', 'outputformat': 'application/json'}
            response = requests.post(CALAIS_URL, text, headers=headers, timeout=80)

            if response.status_code == 200:
                return CalaisResponse(json.loads(response.text))

        except Exception, e:
            print 'Error in Calais connection: ', e


# Calais response manipulations
class CalaisResponse:

    def __init__(self, json_response):
        self.json_response = json_response

    def get_tags(self, limit=8, score_limit=0.3):
        entries = self.json_response
        tags = []

        for entry_key in entries:
            entry = entries[entry_key]

            if '_typeGroup' in entry and entry['_typeGroup'] == 'entities':
                if entry['relevance'] >= score_limit:
                    tags.append({
                        'name': entry['name'],
                        'score': entry['relevance']
                    })

        return self.list_to_array(tags, limit)

    def get_categories(self, limit=4, score_limit=0.3):
        entries = self.json_response
        categories = []

        for entry_key in entries:
            entry = entries[entry_key]

            if '_typeGroup' in entry and entry['_typeGroup'] == 'topics':
                if entry['score'] >= score_limit:
                    categories.append({
                        'name': entry['name'].replace('_', ' '),
                        'score': entry['score']
                    })

        return self.list_to_array(categories, limit)

    def list_to_array(self, input_list, limit):
        array = []
        input_list = sorted(input_list, key=lambda k: k['score'], reverse=True)

        for item in input_list:
            array.append(item['name'])
        return array[:limit]
